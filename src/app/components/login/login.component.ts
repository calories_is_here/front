import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserData = {
    username: '',
    password: ''
  };

  constructor(private auth: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  loginUser(): void {
    this.auth.loginUser(this.loginUserData)
      .subscribe(res => {
          console.log(res);
          localStorage.setItem('token', res.authenticationToken);
          localStorage.setItem('username', res.username);
          localStorage.setItem('refreshToken', res.refreshToken);
          localStorage.setItem('expiresAt', res.expiresAt);
          this.router.navigate(['/nutrients']);
        }
        , err => console.log(err));
  }

}
