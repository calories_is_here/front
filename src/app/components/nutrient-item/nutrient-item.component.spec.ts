import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutrientItemComponent } from './nutrient-item.component';

describe('NutrientItemComponent', () => {
  let component: NutrientItemComponent;
  let fixture: ComponentFixture<NutrientItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutrientItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutrientItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
