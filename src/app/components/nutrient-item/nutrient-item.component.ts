import {Component, Input, OnInit} from '@angular/core';
import {Nutrient} from '../../models/Nutrient';

@Component({
  selector: 'app-nutrient-item',
  templateUrl: './nutrient-item.component.html',
  styleUrls: ['./nutrient-item.component.css']
})
export class NutrientItemComponent implements OnInit {

  @Input() nutrient: Nutrient | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
