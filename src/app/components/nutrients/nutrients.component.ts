import {Component, OnInit} from '@angular/core';
import {NutrientService} from '../../services/nutrient.service';

import {Nutrient} from '../../models/Nutrient';

@Component({
  selector: 'app-nutrients',
  templateUrl: './nutrients.component.html',
  styleUrls: ['./nutrients.component.css']
})
export class NutrientsComponent implements OnInit {

  nutrients: Nutrient[] = [];

  constructor(private nutrientService: NutrientService) {
  }

  ngOnInit(): void {
    this.nutrientService.getNutrients()
      .subscribe(nutrients => {
        this.nutrients = nutrients;
      }, error => console.log(error));
  }

}
