import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerUserData = {
    username: '',
    password: '',
    email: ''
  };

  constructor(private auth: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  registerUser(): void {
    this.auth.registerUser(this.registerUserData)
      .subscribe(res => {
          console.log(res);
          this.router.navigate(['/login']);
        }
        , err => console.log(err));
  }
}
