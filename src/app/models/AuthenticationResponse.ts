export class AuthenticationResponse {
  authenticationToken: string;
  refreshToken: string;
  expiresAt: string;
  username: string;

  constructor(authenticationToken: string, refreshToken: string, expiresAt: string, username: string) {
    this.authenticationToken = authenticationToken;
    this.refreshToken = refreshToken;
    this.expiresAt = expiresAt;
    this.username = username;
  }
}
