export class Nutrient {
  id: number;
  name: string;
  proteins: number;
  fats: number;
  carbs: number;
  kilocalories: number;

  constructor(id: number, name: string, proteins: number, fats: number, carbs: number, kilocalories: number) {
    this.id = id;
    this.name = name;
    this.proteins = proteins;
    this.fats = fats;
    this.carbs = carbs;
    this.kilocalories = kilocalories;
  }
}
