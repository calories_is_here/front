import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User';
import {Observable} from 'rxjs';
import {Message} from '../models/Message';
import {LoginRequest} from '../models/LoginRequest';
import {AuthenticationResponse} from '../models/AuthenticationResponse';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authUrl = 'http://localhost:8091/api/v1/auth';

  constructor(private http: HttpClient, private router: Router) { }

  registerUser(user: User): Observable<Message> {
    return this.http.post<Message>(`${this.authUrl}/register`, user);
  }

  loginUser(login: LoginRequest): Observable<AuthenticationResponse> {
    return this.http.post<AuthenticationResponse>(`${this.authUrl}/login`, login);
  }

  loggedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('username');
    localStorage.removeItem('expiresAt');
    this.router.navigate(['/events']);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

}
