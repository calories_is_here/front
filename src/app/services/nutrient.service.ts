import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Nutrient} from '../models/Nutrient';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NutrientService {

  nutrientUrl = 'http://localhost:8091/api/v1/nutrients';

  constructor(private http: HttpClient) {
  }

  getNutrients(): Observable<Nutrient[]> {
    return this.http.get<Nutrient[]>(this.nutrientUrl);
  }
}
